;;; auto-id.el --- Auto-generate CUSTOM_IDs for Org headlines

;;; Commentary:
;; Enable `auto-id-minor-mode' in the Org buffers in which you require
;; CUSTOM_IDs to be generated.
;;
;; Disable `auto-id-ignore-existing' if you wish to override existing
;; CUSTOM_IDs; leave it enabled if you wish to mix manually-set
;; CUSTOM_IDs with automatically generated ones.
;;
;; You can customize -
;; * how CUSTOM_IDs are generated - see `auto-id-transformers'
;; * how CUSTOM_IDs are made unique - see `auto-id-uniquify-functions'

(require 'dash)

;;; Code:

(defgroup auto-id nil
  "Generate custom IDs for Org headlines."
  :prefix "auto-id-")

(defcustom auto-id-ignore-existing t
  "Whether to update existing custom IDs."
  :type 'boolean)

(defcustom auto-id-words-to-remove '("a" "an" "the" "and" "for" "with" "in" "of" "at")
  "Words to remove when using headline titles to create custom IDs."
  :type '(repeat string))

(defun auto-id-headline-title (headline)
  "Return title of HEADLINE.
HEADLINE must be an Org headline as returned by e.g.
`org-element-at-point'."
  (org-element-property :raw-value headline))

(defun auto-id-remove-brackets (string)
  "Return STRING with any text in [brackets] removed."
  (replace-regexp-in-string "\\[.*\\]" "" string))

(defun auto-id-remove-words (string)
  "Return STRING with some common words removed."
  (let ((regex (cl-loop for word in auto-id-words-to-remove
                 collect `(and bow ,word eow) into clauses
                 finally return (rx-to-string `(or ,@clauses)))))
    (replace-regexp-in-string regex "" string)))

(defun auto-id-remove-whitespace (string)
  "Return STRING with leading and trailing spaces removed."
  (replace-regexp-in-string "\\(^[^a-z]\\|[^a-z]$\\)" "" string))

(defun auto-id-use-dashes (string)
  "Replace spaces and slashes in STRING with dashes."
  (replace-regexp-in-string "[ /]" "-" string))

(defun auto-id-remove-characters (string)
  "Return STRING with quotes and colons removed."
  (replace-regexp-in-string "[':\"*=\\.()]" "" string))

(defun auto-id-squash-dashes (string)
  "Return STRING with excess hyphens removed."
  (replace-regexp-in-string "-+" "-" string))

(defcustom auto-id-transformers '(auto-id-headline-title
                           downcase
                           auto-id-remove-brackets
                           auto-id-remove-words
                           auto-id-remove-whitespace
                           auto-id-use-dashes
                           auto-id-remove-characters
                           auto-id-squash-dashes)
  "Functions to return a custom ID given an Org headline.

Each element must be a unary function. The first function is
called with the Org headline as returned by
`org-element-at-point'."
  :type '(repeat function))

(defun auto-id-run-transformers (transformers arg)
  "Run TRANSFORMERS with ARG.
TRANSFORMERS should be a list of functions (F₁ ... Fₙ), each of
which should accept a single argument.

Call F₁ with ARG, with each following function being called with
the return value of the previous function.

Return the value returned by Fₙ."
  (if transformers
      (cl-loop with first = t
        for fn in transformers
        if (or first arg)
        do (setq arg (funcall fn arg)
                 first nil)
        else do (warn "auto-id: transformer %s was called with nil" fn)
        finally (cl-return arg))
    arg))

;; (defun auto-id-uniquify-lineage (custom-id hash-table)
;;   (let ((new-id ()))
;;     ;; (if (auto-id-unique-p new-id title-id-ht)
;;     ;;     )
;;     ))
;; (cl-loop for elt in (first (org-element-lineage (org-element-parse-buffer 'heading)  t))
;;   when  (and (listp elt) (eq 'headline (first elt)))
;;   collect (org-element-property :title elt))

(defun auto-id-uniquify-by-number (custom-id used-ids)
  "Return a version of CUSTOM-ID not present in USED-IDS.
CUSTOM-ID must be a string, and USED-IDS must be a list of
strings.

CUSTOM-ID is made unique by adding or incrementing a numerical
suffix."
  (cl-loop with new-id = custom-id
    while (member new-id used-ids) do
    (let ((numeric-suffix (progn (string-match "\\(.+-\\)\\([0-9]+\\)$" new-id)
                                 (match-string 2 new-id)))
          (prefix (match-string 1 new-id)))
      (setq new-id
            (if (and prefix numeric-suffix)
                (format "%s%s" prefix (1+ (string-to-number numeric-suffix)))
              (format "%s-1" custom-id))))
    finally return new-id))

(defcustom auto-id-uniquify-functions '(auto-id-uniquify-by-number)
  "A list of functions to ensure uniqueness of a custom-id.

Each function is called with two arguments - the non-unique
custom-id, and a list of used CUSTOM_IDs.

Functions are called in order, and the first non-nil return value
is used as the CUSTOM_ID. (see
`run-hook-with-args-until-success')"
  :type '(repeat functions))

(defun auto-id-org-map-buffer (function)
  "Call FUNCTION for every Org headline in the current buffer."
  (org-map-region function (point-min) (point-max)))

;;;###autoload
(defun auto-id-this-buffer ()
  "Insert or update CUSTOM_IDs in current (Org) buffer.
Affected by `auto-id-ignore-existing' and `auto-id-transformers'."
  (interactive)
  (let (used-ids)
    ;; (message "used ids: %s" used-ids)
    (auto-id-org-map-buffer
     (lambda ()
       (let* ((headline  (org-element-at-point))
              (custom-id (org-element-property :CUSTOM_ID headline)))
         (if (and auto-id-ignore-existing custom-id)
             (push custom-id used-ids)
           (let* ((generated-id         (auto-id-run-transformers auto-id-transformers headline))
                  (generated-id-empty-p (string-empty-p generated-id))
                  (unique-id
                   (if (member generated-id used-ids)
                       (run-hook-with-args-until-success 'auto-id-uniquify-functions
                                                         generated-id
                                                         used-ids)
                     generated-id)))
             (unless generated-id-empty-p
               (org-set-property "CUSTOM_ID" unique-id)
               (push unique-id used-ids)))))))))

;;;###autoload
(define-minor-mode auto-id-mode nil
  :init-value nil :lighter nil :keymap nil
  (if auto-id-mode
      (add-hook 'before-save-hook #'auto-id-this-buffer nil t)
    (remove-hook 'before-save-hook #'auto-id-this-buffer t)))

(provide 'auto-id)

;;; auto-id.el ends here

;; Local Variables:
;; nameless-current-name: "auto-id"
;; End:
