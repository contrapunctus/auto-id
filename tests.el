(defmacro auto-id-with-test-buffer (&rest body)
  "Evaluate BODY in an empty, temporary Org buffer."
  (declare (indent defun) (debug t))
  `(with-current-buffer (get-buffer-create "auto-id-test")
     (delete-region (point-min) (point-max))
     (org-mode)
     (goto-char (point-min))
     ,@body))

(ert-deftest auto-id-no-existing-ids ()
  (should
   (equal '("heading-cookie" "heading-spaces"
            "heading-2" "heading-1" "heading")
          (auto-id-with-test-buffer
            (insert "* heading\n" "** heading\n" "** heading\n")
            (insert "* heading with spaces\n" "* heading with cookie [100%]")
            (auto-id-this-buffer)
            (org-property-values "CUSTOM_ID")))))

(ert-deftest auto-id-existing-ids ()
  (should
   (equal '("heading-cookie" "heading-spaces" "another-heading" "foo")
          (let ((auto-id-ignore-existing t) custom-ids)
            (auto-id-with-test-buffer
              (insert "* heading\n")
              (org-set-property "CUSTOM_ID" "foo")
              (insert "* another heading\n"
                      "* heading with spaces\n"
                      "* heading with cookie [100%]")
              (auto-id-this-buffer)
              (org-property-values "CUSTOM_ID"))))))

;; Local Variables:
;; nameless-current-name: "auto-id"
;; End:
